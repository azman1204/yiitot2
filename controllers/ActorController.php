<?php
namespace app\controllers;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
class ActorController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    public function actionTestUtil() {
        echo unique_id();
        $ymd = \app\utility\Utility::dmy2ymd('28/09/2021');
        echo $ymd;
    }

    // list all actors. index.php?r=actor/list
    public function actionList() {
        $req = \Yii::$app->request;

        if ($req->isPost) {
            // search
            $name = $req->post('name');
            $query = \app\models\Actor::find()->where(['like', 'first_name', "%$name%", false]);
        } else {
            // click dari link / all
            $query = \app\models\Actor::find();
        }
        
        $recordCount = $query->count();
        $pagination = new Pagination(['totalCount' => $recordCount, 'defaultPageSize' => 10]);
        $actors = $query->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('list', ['actors' => $actors, 'pagination' => $pagination]);
    }

    // show form
    public function actionCreate() {
        $actor = new \app\models\Actor();
        return $this->render('form', ['actor' => $actor]);
    }

    public function actionSave() {
        $req = Yii::$app->request;
        $id = $req->post('actor_id');

        if (empty($id)) {
            // insert
            $actor = new \app\models\Actor(); // create a new actor
        } else {
            // update
            $actor = \app\models\Actor::findOne($id);
        }
        
        $actor->first_name = $req->post('first_name');
        $actor->last_name = $req->post('last_name');

        if ($actor->validate()) {
            // validation ok
            $actor->save(); // insert / update into actor
            return $this->redirect('index.php?r=actor/list');
        } else {
            // validation tak ok
            return $this->render('form', ['actor' => $actor]);
        }        
    }

    // index.php?r=actor/delete&actor_id=5
    public function actionDelete($actor_id) {
        echo $actor_id;
        \app\models\Actor::findOne($actor_id)->delete(); // findOne($id) = cari by primary key, return an obj
        return $this->redirect(['actor/list']);
    }

    public function actionEdit($actor_id) {
        $actor = \app\models\Actor::findOne($actor_id);
        return $this->render('form', ['actor' => $actor]);
    }
}