<?php
namespace app\controllers;
use yii\web\Controller;
use app\models\User2;

class HelloController extends Controller {
    // yiitot2.test/index.php?r=hello/list-user
    public function actionListUser() {
        $users = User2::find()->all(); // select * from user
        foreach ($users as $user) {
            echo $user->name . '<br>';
        }
    }

    public function actionWorld() {
        echo 'Hello World';
    }

    // yiitot2.test/index.php?r=hello/myform
    // localhost/yiitot2/web/index.php?r=hello/myform
    public function actionMyform() {
        return $this->render('myform'); // views/hello/myform.php
    }

    public function actionSave() {
        echo \Yii::$app->request->post('name');
    }

    
}