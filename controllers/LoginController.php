<?php
namespace app\controllers;
use app\models\Staff;

class LoginController extends \yii\web\Controller {
    public function actionForm() {
        $staff = new Staff();
        return $this->render('form', compact('staff'));
    }

    public function actionAuth() {
        $req = \Yii::$app->request;
        $staff = new Staff();
        $staff->load($req->post()); // mapkan semua data dari form ke model
        if ($staff->validate()) {
            // check user wujud atau tak
            $staff2 = Staff::find()->where(['username' => $staff->username])->one();
            if (! $staff2) {
                // user tak wujud
                \Yii::$app->session->setFlash('err', 'User does not exist');
                return $this->redirect(['login/form']);
            } else {
                // check password
                if (\Yii::$app->security->validatePassword($staff->password, $staff2->password)) {
                    // password betul
                    \Yii::$app->user->login($staff2); // register dlm session
                    return $this->redirect(['actor/list']);
                } else {
                    // password salah
                    \Yii::$app->session->setFlash('err', 'Password does not match');
                    return $this->redirect(['login/form']);
                }
            }
        } else {
            //  tak taklepas validation. show login form semula
            return $this->render('form', compact('staff'));
        }
    }
}