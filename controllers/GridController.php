<?php
namespace app\controllers;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Customer;

class GridController extends Controller {
    // index = nama default function
    public function actionIndex() {
        $query = Customer::find();
        $dp = new ActiveDataProvider([
            'query' => $query
        ]);
        return $this->render('index', ['dataProvider' => $dp]);
    }
}