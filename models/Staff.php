<?php
namespace app\models;

class Staff extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {
    
    public function rules() {
        return [
            [['username', 'password'], 'required'],
        ];
    }

    public static function primaryKey() {
        return ['username'];
    }

    public static function findIdentity($id) {
        return self::findOne($id); // search by PK
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    public static function findByUsername($username) {
        return null;
    }

    public function getId() {
        return $this->username;
    }

    public function getAuthKey() {
        return true;
    }

    public function validateAuthKey($authKey) {
        return true;
    }
}