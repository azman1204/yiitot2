<?php
namespace app\models;

class Actor extends \yii\db\ActiveRecord {
    // untuk validation
    public function rules() {
        return [
            [['first_name', 'last_name'], 'required'],
            ['first_name', 'string', 'min' => 3]
        ];
        // required = x boleh empty
    }
}