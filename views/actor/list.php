<?php
use yii\helpers\Url;
?>

<h1>Actor List</h1>

<form action="index.php?r=actor/list" method='post'>
    <input type="hidden" name="_csrf" value="<?= \Yii::$app->request->csrfToken ?>">
    Searh by Name : <input type="text" name="name">
    <input type='submit'>
</form>

<table class="table table-striped">
    <tr>
        <td>#</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Action</td>
    </tr>
    <?php
    $no = $pagination->getPage() * $pagination->getPageSize() + 1; 
    foreach($actors as $actor) : ?>
    <tr>
        <td><?= $no++ ?></td>
        <td><?= $actor->first_name ?></td>
        <td><?= $actor->last_name ?></td>
        <td>
            <a href="<?= Url::to(['actor/edit', 'actor_id' => $actor->actor_id]) ?>" class="btn btn-primary">Edit</a>
            <a href="<?= Url::to(['actor/delete', 'actor_id' => $actor->actor_id]) ?>" onclick="return confirm('Are you sure you want to delete')"class="btn btn-danger">Delete</a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<?= \yii\widgets\LinkPager::widget([
    'pagination'   => $pagination,
    'pageCssClass' => 'page-item',
    'linkOptions'  => ['class' => 'page-link']
    ]) ?>