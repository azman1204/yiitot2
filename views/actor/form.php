<?php 
use yii\helpers\Url;
?>

<h1>Actor Form</h1>

<?php if ($actor->hasErrors()): ?>
    <div class="alert alert-danger">
        <?= \yii\helpers\Html::errorSummary($actor) ?>
    </div>
<?php endif; ?>

<form action="<?= Url::to(['actor/save']) ?>" method="post">
    <input type="hidden" name="_csrf" value="<?= \Yii::$app->request->csrfToken ?>">
    <input type="hidden" name="actor_id" value="<?= $actor->actor_id ?>">

    First Name : <input type="text" name="first_name" value="<?= $actor->first_name ?>">
    <br>
    Last Name : <input type="text" name="last_name" value="<?= $actor->last_name ?>">
    <br>
    <input type="submit" value="Save" >
</form>