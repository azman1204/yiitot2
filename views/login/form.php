<h1>Login Form</h1>

<?= \Yii::$app->security->generatePasswordHash('1234') ?>

<?php if (\Yii::$app->session->hasFlash('err')): ?>
    <div class="alert alert-danger">
        <?= \Yii::$app->session->getFlash('err') ?>
    </div>
<?php endif; ?>

<form action="<?= \yii\helpers\Url::to(['login/auth']) ?>" method="post">
    <input type="hidden" name="_csrf" value="<?= \Yii::$app->request->csrfToken ?>">
    User ID : <input type='text' name='Staff[username]' value='<?= $staff->username ?>'>
    <br>
    Password : <input type='password' name='Staff[password]'>
    <br>
    <input type='submit' value="Login">
</form>