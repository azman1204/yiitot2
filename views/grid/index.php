<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'first_name', 
        'last_name',
        'email'
    ]
]);
?>