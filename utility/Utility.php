<?php
namespace app\utility;

class Utility {
    // 10/11/2021
    static function dmy2ymd($dmy) {
        $day = substr($dmy, 0,2);
        $month = substr($dmy, 3,2);
        $year = substr($dmy, 6,4);
        return "$year-$month-$day";
    }
}